#ifndef SKIPLIST_H
#define SKIPLIST_H

#include <stdlib.h>

#define SUCCESS 0
#define ALLOCATE_ERROR 1

typedef struct infoNode* skiplist;
typedef struct listNode* slIterator;

int Skiplist_initialize(skiplist *lst, int MaxNumofPointers, int MaxValue);

int Skiplist_create(skiplist lst);

int Skiplist_insert(skiplist lst, int searchKey, void *value);

int Skiplist_delete(skiplist lst, int searchKey);

void *SkipList_search(skiplist lst, int searchKey, slIterator *it);

void SkipList_destroy(skiplist *lst, void (*deleteObject)(void **object));

void SkipList_initIterator(skiplist lst, slIterator *it);

void *SkipList_nextValue(slIterator *it);

#endif
