#include "functions.h"

int totalStudents = 0;

int insert(hashPtr studentsHash, skiplist lst, int studid, char *lastname,
           char *firstname, float gpa, int numcourses, char *deprt, int postcode)
{
    /*
     * create a new student record and insert it
     * in the skiplist and the hashTable
     */

    //there is a student with the same id
    if(SkipList_search(lst, studid, NULL) != NULL)
        return RECORD_EXISTS;

    //create a new student
    studentPtr newStudent;
    StudentRecord_create(&newStudent, studid, firstname, lastname,
                                         gpa, numcourses, deprt,postcode);
    //insert student in skiplist
    Skiplist_insert(lst, studid, newStudent);

    int blockExists = 1;
    studentsBlockPtr studentsBlock;
    studentsBlock = Hash_search(studentsHash, postcode, StudentsBlock_getPostcode);

    //students block not found, create a new block
    if(studentsBlock == NULL)
    {
        StudentsBlock_create(&studentsBlock, postcode);
        blockExists = 0;
    }

    //add student in the block
    StudentsBlock_addStudent(studentsBlock, newStudent);

    if(blockExists == 0)    //insert the new block in the hash table
        Hash_insert(studentsHash, postcode, studentsBlock);
    totalStudents++;
    //print student record to stdout
    StudentRecord_print(newStudent);
    return SUCCESS;
}

int query(skiplist lst, int studid)
{
    /*
     * find the record and print it to stdout
     * or print not found to stderr
     */

    void *record = SkipList_search(lst, studid, NULL);
    if(record == NULL)
    {
        fprintf(stderr, "not found\n");
        return NOT_FOUND;
    }
    else
    {
        StudentRecord_print((studentPtr) record);
        return SUCCESS;
    }
}


int modify(skiplist lst, int studid, float gpa, int numcourses)
{
    /*
     * change gpa and number of courses
     */
    studentPtr student = SkipList_search(lst, studid, NULL);
    if(student == NULL)
        return NOT_FOUND;
    else
    {
        StudentRecord_setGPA(student, gpa);
        StudentRecord_setNumCources(student, numcourses);
        StudentRecord_print(student);
        return SUCCESS;
    }
}

int delete(hashPtr studentstHash, skiplist lst, int studid)
{
    /*
     * delete the student with id
     * equal to studid
     */
    studentPtr student = SkipList_search(lst, studid, NULL);
    if(student == NULL)
    {
        fprintf(stderr, "not found\n");
        return NOT_FOUND;
    }
    else
    {
        //remove from skiplist
        Skiplist_delete(lst, studid);
        //remove from hash
        int postcode = StudentRecord_getPostcode(student);
        //find the block that the student is saved
        studentsBlockPtr studentsBlock;
        studentsBlock = Hash_search(studentstHash, postcode, StudentsBlock_getPostcode);
        //remove student from the block
        StudentsBlock_deleteStudent(studentsBlock, student);
        //if students block is empty, remove it from the hash table
        if(StudentsBlock_getCount(studentsBlock) == 0)
        {
            Hash_delete(studentstHash, postcode, studentsBlock,
                                              StudentsBlock_comparePostcode);
            StudentsBlock_destroy(((void **)&studentsBlock));
        }
        //print student's information
        StudentRecord_print(student);
        StudentRecord_delete((void **)&student);
        totalStudents--;
        return SUCCESS;
    }
}


int raverage(skiplist studentsList, int studida, int studidb)
{
    /*
     * calculate the gpa of all the students that
     * their id is studida <= id <= studidb
     */
    slIterator it; int count = 0; float sum = 0.0; studentPtr student;

    SkipList_search(studentsList, studida, &it);
    while((student = SkipList_nextValue(&it)) != NULL)
    {
        if (StudentRecord_getID(student) > studidb)
            break;
        count++;
        sum += StudentRecord_getGPA(student);
    }
    if(count > 0)
    {
        printf("%.2f\n", sum / count);
        return SUCCESS;
    }
    else
    {
        fprintf(stderr, "not found\n");
        return FAILED;
    }

}

int average(hashPtr studentsHash, int postcode)
{
    /*
     * calculate the gpa of all the students
     * that theirs postcode is equal to
     * variable postcode
     */
    int count = 0; float sum = 0.0;
    studentsBlockPtr studentsBlock; studentPtr student;

    studentsBlock = Hash_search(studentsHash, postcode,
                                         StudentsBlock_getPostcode);
    if(studentsBlock == NULL)
        return NOT_FOUND;
    studentIterator it;
    StudentsBlock_initIterator(studentsBlock, &it);
    while(it != NULL)
    {
        student = StudentsBlock_nextValue(&it);
        sum += StudentRecord_getGPA(student);
        count++;
    }
    if(count > 0)
        printf("%.2f\n", sum / count);
    else
        fprintf(stderr, "not found\n");

    return SUCCESS;
}

void bubblesort(studentPtr *studentArray, int n)
{
    //sort array in descending order
    int newN, i; void *temp;
    do
    {
        newN = 0;
        for(i = 1; i < n; i++)
            if(StudentRecord_getGPA(studentArray[i -1]) <
                    StudentRecord_getGPA(studentArray[i]) )
            {
                temp = studentArray[i -1];
                studentArray[i -1] = studentArray[i];
                studentArray[i] = temp;
                newN = i;
            }
        n = newN;
    }
    while(n != 0);
}

int taverage(hashPtr studentsHash, int k, int postcode)
{
    /*
     * print k best students that live in postcode
     */
    studentPtr *studentsArray; int i = 0, studentsNumber;
    studentsBlockPtr studentsBlock; studentPtr student; studentIterator it;

    //find the block of students that live in postcode
    studentsBlock = Hash_search(studentsHash, postcode, StudentsBlock_getPostcode);
    if(studentsBlock == NULL)
    {
        fprintf(stderr, "not found\n");
        return FAILED;
    }

    studentsNumber = StudentsBlock_getCount(studentsBlock);
    if((studentsArray = malloc(sizeof(studentPtr) * studentsNumber)) == NULL)
        return ALLOCATE_ERROR;

    //save all students in studentsArray
    StudentsBlock_initIterator(studentsBlock, &it);
    while((student = StudentsBlock_nextValue(&it)) != NULL)
        studentsArray[i++] = student;

    //if k < studentsNumber print all students that live in postcode
    if(k > studentsNumber)
        k = studentsNumber;
    //sort the array in descending order
    bubblesort(studentsArray, studentsNumber);
    //print k best students
    for(i = 0; i < k; i++)
        StudentRecord_print(studentsArray[i]);

    free(studentsArray);
    return SUCCESS;
}

int bottom(skiplist studentsList, int k)
{
    int index = 0; float maxGPA = 0.0, studentGPA; int maxIndex = 0, i;

    studentPtr *minStudents, student;
    //create an array to store k students
    if((minStudents = malloc(sizeof(studentPtr) * k)) == NULL)
        return ALLOCATE_ERROR;

    slIterator it;
    SkipList_initIterator(studentsList, &it);
    while((student = SkipList_nextValue(&it)) != NULL)
    {
        studentGPA = StudentRecord_getGPA(student);
        //if minStudents array is empty
        if(index < k)
        {
            if(studentGPA > maxGPA)
            {
                maxGPA = studentGPA;
                maxIndex = index;
            }
            minStudents[index] = student;
            index++;
        }
        else
        {
            if(studentGPA < maxGPA)
            {
                minStudents[maxIndex] = student;
                maxGPA = 0;
                maxIndex = 0;
                for (i = 0; i < k; i++) {
                    if (StudentRecord_getGPA(minStudents[i]) > maxGPA) {
                        maxGPA = StudentRecord_getGPA(minStudents[i]);
                        maxIndex = i;
                    }
                }
            }
        }
    }
    for(i = 0; i < index; i++)
        StudentRecord_print(minStudents[i]);
    free(minStudents);
    return SUCCESS;
}

int courses_to_take(hashPtr studentsHash, int postcode, char *deprt)
{
    int totalCourses = 0, found = 0;
    studentsBlockPtr studentsBlock; studentPtr student; studentIterator it;

    studentsBlock = Hash_search(studentsHash, postcode, StudentsBlock_getPostcode);
    if(studentsBlock == NULL)
        return NOT_FOUND;

    StudentsBlock_initIterator(studentsBlock, &it);
    while(it != NULL)
    {
        student = StudentsBlock_nextValue(&it);
        if(strcmp(StudentRecord_getDeprt(student), deprt) == 0)
        {
            totalCourses += StudentRecord_getCourses(student);
            StudentRecord_print(student);
            found++;
        }
    }
    if(found > 0)
        printf("%d\n", totalCourses);
    else
        fprintf(stderr, "not found\n");
    return SUCCESS;
}

int find(skiplist studentsList, float gpa)
{
    int maxCourses = -1, courses; slIterator it; studentPtr student;
    //find max number of courses
    SkipList_initIterator(studentsList, &it);
    while((student = SkipList_nextValue(&it)) != NULL)
    {
        courses = StudentRecord_getCourses(student);
        if(StudentRecord_getGPA(student) > gpa && courses > maxCourses)
            maxCourses = courses;
    }

    if(maxCourses == -1)
        fprintf(stderr, "not found\n");
    else
    {
        SkipList_initIterator(studentsList, &it);
        while ((student = SkipList_nextValue(&it)) != NULL)
        {
            courses = StudentRecord_getCourses(student);
            if (StudentRecord_getGPA(student) > gpa && courses == maxCourses)
                StudentRecord_print(student);
        }
    }
    return SUCCESS;
}

int percentile(hashPtr studentsHash, int postcode)
{
    studentsBlockPtr studentsBlock;
    studentsBlock = Hash_search(studentsHash, postcode, StudentsBlock_getPostcode);
    if(studentsBlock == NULL)
        return NOT_FOUND;
    int postcodeStudents;
    postcodeStudents = StudentsBlock_getCount(studentsBlock);
    float percentage = (float) postcodeStudents / totalStudents;
    printf("%.2f\n", percentage);
    return SUCCESS;
}

int percentiles(hashPtr studentsHash)
{
    int postcode, i, hashentries = Hash_getNumofHashentries(studentsHash);
    htIterator htIt; void *value;
    for(i = 0; i < hashentries; i++)
    {
        Hash_initIterator(studentsHash, &htIt, i);
        while((value = Hash_nextValue(&htIt)) != NULL)
        {
            postcode = StudentsBlock_getPostcode(value);
            printf("%d ", postcode);
            percentile(studentsHash, postcode);
        }
    }
    return SUCCESS;
}

void exitP(hashPtr *studentsHash, skiplist *studentsList)
{
    if(*studentsList != NULL)
        SkipList_destroy(studentsList, NULL);
    if(*studentsHash != NULL)
        Hash_destroy(studentsHash, StudentsBlock_destroy);
}

