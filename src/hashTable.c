#include "hashTable.h"

typedef struct hashTable
{
    int count;
    int hashentries;
    list *buckets;
} hashTable;


int Hash_create(hashPtr *hptr, int hashentries)
{
    /*
     * create a new hashTable
     */
    if((*hptr = malloc(sizeof(hashTable))) == NULL)
        return ALLOCATE_ERROR;

    //create an array of lists
    if(((*hptr)->buckets = malloc(sizeof(list) * hashentries)) == NULL)
    {
        free(*hptr);
        return ALLOCATE_ERROR;
    }
    (*hptr)->hashentries = hashentries;
    (*hptr)->count = 0;

    //create a list for each entry of the hashTable
    int i;
    for(i = 0; i < hashentries; i++)
        List_create(&(*hptr)->buckets[i]);
    return 0;
}


void Hash_destroy(hashPtr *hptr, void (*deleteObject)(void **))
{
    /*
     * destroy all lists of the hashTable
     * and the hashTable struct
     * delete objects if function deleteObject isn't NULL
     */
    int i;
    for(i = 0; i < (*hptr)->hashentries; i++)
        List_destroy(&(*hptr)->buckets[i], deleteObject);
    //free buckets array and hashTable struct
    free((*hptr)->buckets); free(*hptr);
    *hptr = NULL;
}


int Hash_insert(hashPtr hptr, int key, void *value)
{
    int index = Hash_hashFunction(hptr, key);
    return List_addFirst(hptr->buckets[index], value);
}


void *Hash_search(hashPtr hptr, int searchkey, int (*getKey)(void *))
{
    int index = Hash_hashFunction(hptr, searchkey);
    list lst = hptr->buckets[index];

    void *value;
    listIterator it;
    List_initIterator(lst, &it);
    while(it != NULL)
    {
        value = List_returnValue(&it);
        if((*getKey)(value) == searchkey)
            return value;
    }
    return NULL;
}


void Hash_delete(hashPtr hptr, int key, void *value, int (*compare)(void *, void *))
{
    int index = Hash_hashFunction(hptr, key);
    List_remove(hptr->buckets[index], value, compare);
}


int Hash_getCount(hashPtr hptr)
{
    return hptr->count;
}


int Hash_hashFunction(hashPtr hptr, int value)
{
    return value % hptr->hashentries;
}


int Hash_getNumofHashentries(hashPtr hptr)
{
    return hptr->hashentries;
}


void Hash_initIterator(hashPtr hptr, htIterator *htIt, int index)
{
    List_initIterator(hptr->buckets[index], htIt);
}


void *Hash_nextValue(htIterator *htIt)
{
    return List_returnValue(htIt);
}

