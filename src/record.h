#ifndef _RECORD_H
#define _RECORD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

#define SUCCESS 0
#define ALLOCATE_ERROR 1

typedef struct studentRecord* studentPtr;
typedef struct studentsBlock* studentsBlockPtr;

typedef listIterator studentIterator;


/*
 * Students block
 */

int StudentsBlock_create(studentsBlockPtr *studentsBl, int postcode);

void StudentsBlock_addStudent(studentsBlockPtr studentsBl, studentPtr student);

void StudentsBlock_deleteStudent(studentsBlockPtr studentsBl, studentPtr student);

void StudentsBlock_destroy(void **studentsBl);

int StudentsBlock_getPostcode(void *studentsBl);

int StudentsBlock_getCount(studentsBlockPtr studentBl);

void StudentsBlock_initIterator(studentsBlockPtr studentBl, studentIterator *it);

void *StudentsBlock_nextValue(studentIterator *it);

int StudentsBlock_comparePostcode(void *value1, void *value2);

/*
 * Student record
 */

int StudentRecord_create(studentPtr *record, int studid, char *firstname, char *lastname,
                         float gpa, int numofcourses, char *deprt, int postcode);

void StudentRecord_delete(void  **record);

void StudentRecord_setGPA(studentPtr student, float gpa);

void StudentRecord_setNumCources(studentPtr student, int numcources);

int StudentRecord_getID(studentPtr student);

int StudentRecord_getPostcode(studentPtr student);

float StudentRecord_getGPA(studentPtr student);

char *StudentRecord_getDeprt(studentPtr student);

int StudentRecord_getCourses(studentPtr student);

void StudentRecord_print(studentPtr student);

int StudentRecord_compareID(void *value1, void *value2);

#endif