#ifndef _HASH_H
#define _HASH_H

#include "list.h"

#define ALLOCATE_ERROR 1

typedef struct hashTable* hashPtr;
typedef listIterator htIterator;


int Hash_create(hashPtr *hptr, int hashentries);

void Hash_destroy(hashPtr *hptr, void (*deleteObject)(void **));

int Hash_insert(hashPtr hptr, int key, void *value);

void Hash_delete(hashPtr hptr, int key, void *value, int (*compare)(void *, void *));

void *Hash_search(hashPtr hptr, int searchkey, int (*getKey)(void *));

int Hash_hashFunction(hashPtr hptr, int value);

int Hash_getCount(hashPtr hptr);

int Hash_getNumofHashentries(hashPtr hptr);

void Hash_initIterator(hashPtr hptr, htIterator *htIt, int index);

void *Hash_nextValue(htIterator *htIt);

#endif