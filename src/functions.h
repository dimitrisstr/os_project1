#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <stdio.h>
#include "skiplist.h"
#include "hashTable.h"
#include "record.h"

#define SUCCESS 0
#define ALLOCATE_ERROR 1
#define FAILED 2
#define RECORD_EXISTS 3
#define NOT_FOUND 4

int insert(hashPtr studentsHash, skiplist lst, int studid, char *lastname,
           char *firstname, float gpa, int numcourses, char *deprt, int postcode);

int query(skiplist lst, int studid);

int modify(skiplist lst, int studid, float gpa, int numcourses);

int delete(hashPtr studentHash, skiplist lst, int studid);

int raverage(skiplist studentsList, int studida, int studidb);

int average(hashPtr studentsHash, int postcode);

int taverage(hashPtr studentsHash, int k, int postcode);

int bottom(skiplist studentsList, int k);

int courses_to_take(hashPtr studentsHash, int postcode, char *deprt);

int find(skiplist studentsList, float gpa);

int percentile(hashPtr studentsHash, int postcode);

int percentiles(hashPtr studentsHash);

void exitP(hashPtr *studentsHash, skiplist *studentsList);

#endif
