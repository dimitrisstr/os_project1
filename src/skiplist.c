#include "skiplist.h"

typedef struct listNode
{
    int key;
    void *value;
    struct listNode **forward;
} listNode;

typedef struct infoNode
{
    int maxNumofPointers;
    int maxValue;
    int size;
    struct listNode *header;
    struct listNode *terminatingNode;
} infoNode;


listNode* Skiplist_makeNode(listNode *terminatingNode, int numberOfPointers, int searchKey, void *value);

int Skiplist_initialize(skiplist *lst, int MaxNumofPointers, int MaxValue)
{
    //allocate a new infonode and initialize it
    if((*lst = malloc(sizeof(infoNode))) == NULL)
        return ALLOCATE_ERROR;
    (*lst)->maxNumofPointers = MaxNumofPointers;
    (*lst)->maxValue = MaxValue;
    (*lst)->size = 0;
    (*lst)->header = NULL;
    (*lst)->terminatingNode = NULL;
    return SUCCESS;
}

int Skiplist_create(skiplist lst)
{
    //create the first and the last listNode of the skiplist
    listNode *header, *terminatingNode;
    if((header = malloc(sizeof(listNode))) == NULL)
        return ALLOCATE_ERROR;
    if((terminatingNode = malloc(sizeof(listNode))) == NULL)
    {
        free(header);
        return ALLOCATE_ERROR;
    }
    //allocate memory for the forward array
    if((header->forward = malloc(sizeof(listNode*) * lst->maxNumofPointers)) == NULL)
    {
        free(header); free(terminatingNode);
        return ALLOCATE_ERROR;
    }

    if((terminatingNode->forward = malloc(sizeof(listNode*) * lst->maxNumofPointers)) == NULL)
    {
        free(header->forward); free(header); free(terminatingNode);
        return ALLOCATE_ERROR;
    }

    /*
     * initialize pointers
     * header's pointers point to terminatingNode
     * terminatingNode's pointers point to NULL
     */
    int i;
    for(i = 0; i < lst->maxNumofPointers; i++)
    {
        header->forward[i] = terminatingNode;
        terminatingNode->forward[i] = NULL;
    }
    terminatingNode->key = lst->maxValue;
    lst->terminatingNode = terminatingNode;
    lst->header = header;
    terminatingNode->value = NULL;
    header->value = NULL;
    return SUCCESS;
}

int Skiplist_insert(skiplist lst, int searchKey, void *value)
{
    /*
     * insert a new listNode in skip-list
     */
    listNode *x = lst->header, **update;
    if((update = malloc(sizeof(listNode*) * lst->maxNumofPointers)) == NULL)
        return ALLOCATE_ERROR;
    int i, level;
    for(i = lst->maxNumofPointers -1; i >= 0; i--)
    {
        while(x->forward[i]->key < searchKey)
            x = x->forward[i];
        update[i] = x;
    }
    x = x->forward[0];
    if(x->key == searchKey)
        x->value = value;
    else
    {
        level = rand() % lst->maxNumofPointers;
        x = Skiplist_makeNode(lst->terminatingNode, lst->maxNumofPointers, searchKey, value);
        if(x == NULL)   //error
        {
            free(update);
            return ALLOCATE_ERROR;
        }
        for(i = 0; i <= level; i++)
        {
            x->forward[i] = update[i]->forward[i];
            update[i]->forward[i] = x;
        }
        lst->size++;
    }
    free(update);
    return SUCCESS;
}


listNode* Skiplist_makeNode(listNode *terminatingNode, int numberOfPointers, int searchKey, void *value)
{
    /*
     * create and initialize a new listNode
     */
    listNode *newNode;
    if((newNode = malloc(sizeof(listNode))) == NULL)
        return NULL;

    newNode->forward = malloc(sizeof(listNode*) * numberOfPointers);

    //initialize the new listNode
    newNode->key = searchKey;
    newNode->value = value;
    int i;
    for(i = 0; i < numberOfPointers; i++)
        newNode->forward[i] = terminatingNode;
    return newNode;
}

int Skiplist_delete(skiplist lst, int searchKey)
{
    /*
     * find and delete the listNode that contains the key 'searchKey'
     */
    int i, maxLevel = lst->maxNumofPointers;
    listNode **update, *x;
    if((update = malloc(sizeof(listNode*) * maxLevel)) == NULL)
        return ALLOCATE_ERROR;

    x = lst->header;
    for(i = maxLevel -1; i >= 0; i--)
    {
        while (x->forward[i]->key < searchKey)
            x = x->forward[i];
        update[i] = x;
    }

    x = x->forward[0];
    if(x->key == searchKey)
    {
        for(i = 0; i < maxLevel; i++)
        {
            if (update[i]->forward[i] != x)
                break;
            update[i]->forward[i] = x->forward[i];
        }
        free(x->forward);
        free(x);
        lst->size--;
    }
    free(update);
    return SUCCESS;
}

void SkipList_destroy(skiplist *lst, void (*deleteObject)(void **))
{
    /*
     * destroy all listNodes and free data
     * if function deleteObject isn't NULL
     */
    listNode *temp1, *temp2;
    temp1 = (*lst)->header;
    while(temp1 != NULL)
    {
        temp2 = temp1;
        temp1 = temp1->forward[0];
        if(deleteObject != NULL)
            (*deleteObject)(&temp2->value);
        free(temp2->forward);
        free(temp2);
    }
    free(*lst);
    *lst = NULL;
}

void *SkipList_search(skiplist lst, int searchKey, slIterator *it)
{
    /*
     * find a listNode that contains the key 'searchKey'
     * and return an iterator if slIterator 'it' isn't NULL
     */
    listNode *x = lst->header;
    int i;
    for(i = lst->maxNumofPointers -1; i >= 0; i--)
    {
        while(x->forward[i]->key < searchKey)
            x = x->forward[i];
    }
    x = x->forward[0];
    if(it != NULL)
        *it = x;
    if(x->key == searchKey) //key found
        return x->value;
    else                    //key not found
        return NULL;
}

void SkipList_initIterator(skiplist lst, slIterator *it)
{
    //iterator points at the first element of the list
    *it = lst->header->forward[0];
}

void *SkipList_nextValue(slIterator *it)
{
    /*
     * return the data of the listNode that the iterator
     * point's to and move the iterator to the next listNode
     */
    if(*it != NULL)
    {
        void *value = (*it)->value;
        if(value == NULL)   //'it' is the last node
            return NULL;
        (*it) = (*it)->forward[0];
        return value;
    }
    else
        return NULL;
}