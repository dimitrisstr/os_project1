#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "hashTable.h"
#include "skiplist.h"
#include "functions.h"

#define EXIT -1
#define SUCCESS 0
#define PARSE_ERROR 1
#define ARGS_ERROR 2
#define FILE_ERROR 3

//skiplist's arguments
#define MAXLEVEL 5
#define MAXVALUE 1000000

#define BUFFER_SIZE 256

hashPtr studentsHash;
skiplist studentsList;
char *operationsFile = NULL;

int readFromFile(char *filename);
void readFromUser();
int execute(char *command);
int checkPostcode(int postcode);

int main(int argc, char *argv[])
{
    int i, hashentries = 0;
    //check the program's arguments
    for(i = 1; i < argc -1; i++)
    {
        //number of hashTable entries
        if(strcmp(argv[i], "-b") == 0)   //mandatory argument
            hashentries = atoi(argv[i +1]);
        else if(strcmp(argv[i], "-l") == 0) //operations file
        {
            operationsFile = malloc(sizeof(char) * (strlen(argv[i + 1]) + 1));
            strcpy(operationsFile, argv[i +1]);
        }
    }

    if(hashentries == 0)
    {
        printf("usage: ./stucs -l operationsFile -b hashentries -c config-file\n");
        exit(ARGS_ERROR);
    }

    srand((unsigned) time(NULL));
    Hash_create(&studentsHash, hashentries);
    Skiplist_initialize(&studentsList, MAXLEVEL, MAXVALUE);
    Skiplist_create(studentsList);

    //execute commands from file
    if(operationsFile != NULL)
        readFromFile(operationsFile);
    free(operationsFile);
    //execute commands from stdin
    readFromUser();
    exitP(&studentsHash, &studentsList);

    return EXIT_SUCCESS;
}

void readFromUser()
{
    /*
     * read commands from stdin
     */
    char buffer[BUFFER_SIZE];
    printf("> ");
    while(fgets(buffer, BUFFER_SIZE -1, stdin) != NULL)
    {
        if(execute(buffer) == EXIT)
            exit(EXIT_SUCCESS);
        printf("> ");
    }
}


int readFromFile(char *filename)
{
    /*
     * read commands from file
     */
    FILE *inputFile;
    if((inputFile = fopen(filename, "r")) == NULL)
        return FILE_ERROR;
    free(filename);
    char buffer[BUFFER_SIZE];
    while(fgets(buffer, BUFFER_SIZE -1, inputFile) != NULL)
        if(execute(buffer) == EXIT)
        {
            fclose(inputFile);
            exit(EXIT_SUCCESS);
        }
    return SUCCESS;
}

int checkPostcode(int postcode)
{
    if( (postcode % 100000) != postcode)
        return -1;
    else
        return 0;
}

int execute(char *command)
{
    /*
     * parse the command and execute the
     * right function
     */
    char *token, *args;
    if((token  = strtok(command, " \n")) == NULL)   //command
        return PARSE_ERROR;

    args = strtok(NULL, "\n");  //command's arguments

    if(strcmp(token, "i") == 0)
    {
        //i(nsert) studid lastname firstname gpa numcourses deprt postcode
        int studid, numcourses, postcode; float gpa;
        char firstname[BUFFER_SIZE], lastname[BUFFER_SIZE], deprt[BUFFER_SIZE];

        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d %s %s %f %d %s %d", &studid, lastname, firstname,
                    &gpa, &numcourses, deprt, &postcode) != 7)
            return PARSE_ERROR;
        if(checkPostcode(postcode) < 0)
            return PARSE_ERROR;

        insert(studentsHash, studentsList, studid, lastname, firstname, gpa,
                numcourses, deprt, postcode);
    }
    else if(strcmp(token, "q") == 0)
    {
        //q(uery) studid
        int studid;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d", &studid) != 1)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "q %s\n", args);
        query(studentsList, studid);
    }
    else if(strcmp(token, "m") == 0)
    {
        //m(odify) studid gpa numcourses
        int studid, numcources; float gpa;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d %f %d", &studid, &gpa, &numcources) != 3)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "m %s\n", args);
        modify(studentsList, studid, gpa, numcources);
    }
    else if(strcmp(token, "d") == 0)
    {
        //d(elete) studid
        int studid;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d", &studid) != 1)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "d %s\n", args);
        delete(studentsHash, studentsList, studid);
    }
    else if(strcmp(token, "ra") == 0)
    {
        //ra(verage) studida studidb
        int studida, studidb;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d %d", &studida, &studidb) != 2)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "ra %s\n", args);
        raverage(studentsList, studida, studidb);
    }
    else if(strcmp(token, "a") == 0)
    {
        //a(verage) postcode
        int postcode;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d", &postcode) != 1)
            return PARSE_ERROR;
        if(checkPostcode(postcode) < 0)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "a %s\n", args);
        average(studentsHash, postcode);
    }
    else if(strcmp(token, "ta") == 0)
    {
        //ta(verage) k postcode
        int k, postcode;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d %d", &k, &postcode) != 2)
            return PARSE_ERROR;
        if(checkPostcode(postcode) < 0)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "ta %s\n", args);
        taverage(studentsHash, k, postcode);
    }
    else if(strcmp(token, "b") == 0)
    {
        //b(ottom) k
        int k;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d", &k) != 1)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "b %s\n", args);
        bottom(studentsList, k);
    }
    else if(strcmp(token, "ct") == 0)
    {
        //ct(courses-to-take) postcode deprt
        int postcode; char deprt[256];
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d %s", &postcode, deprt) != 2)
            return PARSE_ERROR;
        if(checkPostcode(postcode) < 0)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "ct %s\n", args);
        courses_to_take(studentsHash, postcode, deprt);
    }
    else if(strcmp(token, "f") == 0)
    {
        //f(ind) gpa
        float gpa;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%f", &gpa) != 1)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "f %s\n", args);
        find(studentsList, gpa);
    }
    else if(strcmp(token, "p") == 0)
    {
        //p(ercentile) postcode
        int postcode = 0;
        if(args == NULL)
            return PARSE_ERROR;
        if(sscanf(args, "%d", &postcode) != 1)
            return PARSE_ERROR;
        if(checkPostcode(postcode) < 0)
            return PARSE_ERROR;

        //print command to stderr
        fprintf(stderr, "p %s\n", args);
        percentile(studentsHash, postcode);
    }
    else if(strcmp(token, "pe") == 0)
    {
        //pe(rcentiles)
        //print command to stderr
        fprintf(stderr, "pe\n");
        percentiles(studentsHash);
    }
    else if(strcmp(token, "e") == 0)
    {
        //e(xit)
        //print command to stderr
        fprintf(stderr, "e\n");
        exitP(&studentsHash, &studentsList);
        return EXIT;
    }
    return SUCCESS;
}
