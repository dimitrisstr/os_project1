#ifndef _LIST_H
#define _LIST_H

#include <stdio.h>
#include <stdlib.h>

#define ALLOCATERROR 1

typedef struct infoNode* list;
typedef struct listNode* listIterator;

//create a list
int List_create(list *lst);

//add first
int List_addFirst(list lst, void *data);

//remove the node and return its value
void *List_remove(list lst, void *value, int (*Compare)(void *, void *));

//remove first node and return its value
void *List_removeFirst(list lst);

//destroy all list nodes and free objects if function deleteObject isn't NULL
int List_destroy(list *lst, void (*deleteObject)(void **));

//returns 1 if list is empty
int List_isEmpty(list lst);

//return the size of the list
int List_returnSize(list lst);

//return the value of the node the iterator points to and move the iterator to the next node
void* List_returnValue(listIterator *it);

//initialize the iterator to point at the start of the list
void List_initIterator(list lst, listIterator *it);

#endif
