
#include "list.h"

//data listNode
typedef struct listNode
{
    void *data;
    struct listNode* next;
} listNode;

//information listNode
typedef struct infoNode
{
    int size;
    listNode* start;
} infoNode;


/*
 *  Compare function
 *  int compare(void * value1, void *value2)
 *  {
 *      if(value1 > value2)
 *          return 1;
 *      else if(value1 < value2)
 *          return -1;
 *      else
 *          return 0;
 *  }
 */


int List_create(list *lst)
{
    //allocate a new infoNode
    if((*lst = malloc(sizeof(infoNode))) == NULL)
		return ALLOCATERROR;

    //initialize variables
    (*lst)->size = 0;
    (*lst)->start = NULL;
    return 0;
}

void *List_remove(list lst, void *value, int (*Compare)(void *, void *))
{
    listNode *previous, *current;
    current = lst->start;
    //remove the first listNode
    if((*Compare)(value, current->data) == 0)
        return List_removeFirst(lst);
    else
    {
        while (current != NULL)
        {
            previous = current;
            current = current->next;
            if((*Compare)(current->data, value) == 0) //value found
            {
                previous->next = current->next;
                void *data = current->data;
                free(current);
                lst->size--;
                return data;
            }
        }
    }
	return NULL;
}

int List_addFirst(list lst, void *data)
{
    //add a listNode at the start of the list
    listNode *temp;
    if((temp = malloc(sizeof(listNode))) == NULL)
        return ALLOCATERROR;

    temp->next = lst->start;
    temp->data = data;

    lst->start = temp;
    lst->size++;

    return 0;
}

void *List_removeFirst(list lst)
{
    /*
     *remove the first listNode of the list
     *free ONLY the memory allocated
     *for the listNode and not the data
     *return the data pointer
     */
    if(lst->start == NULL)
        return NULL;
    else
    {
        listNode *temp;
        void *value;
        temp = lst->start;
        lst->start = temp->next;
        value = temp->data;
        free(temp);
        lst->size--;
        return value;
    }
}


int List_destroy(list *lst, void (*deleteObject)(void **))
{
	/*
	 * destroy all list's nodes and delete objects
	 * if function deleteObject isn't NULL
	 */
	listNode *temp1 = (*lst)->start;
	listNode *temp2;
	while(temp1 != NULL)
	{
		temp2 = temp1;
		temp1 = temp1->next;
		if((*deleteObject) != NULL)
            (*deleteObject)(&temp2->data);
		free(temp2);
	}
	free(*lst);
	return 0;
}

int List_isEmpty(list lst)
{
	return lst->size == 0;
}

int List_returnSize(list lst)
{
    //return the size of the list
    return lst->size;
}

void List_initIterator(list lst, listIterator *it)
{
    //initialize the iterator to point at the start of the list
    *it = lst->start;
}

void* List_returnValue(listIterator *it)
{
    /*
     * return the data of the listNode that the iterator
     * points to and move the iterator to the next listNode
     */
    void *data;
    if(*it != NULL)
    {
        data = (*it)->data;
        *it = (*it)->next;
        return data;
    }
    return NULL;
}
