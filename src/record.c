#include "record.h"

typedef struct studentRecord
{
    int studid;
    char *firstname;
    char *lastname;
    float gpa;
    int numofcourses;
    char *deprt;
    int postcode;
} studentRecord;


//students that have the same postcode
typedef struct studentsBlock
{
    int postcode;
    list studentsList;
} studentsBlock;


/*
 * Students block
 */

int StudentsBlock_create(studentsBlockPtr *studentsBl, int postcode)
{
    /*
     * create a new block of students
     */
    if((*studentsBl = malloc(sizeof(studentsBlock))) == NULL)
        return ALLOCATE_ERROR;

    (*studentsBl)->postcode = postcode;
    List_create(&(*studentsBl)->studentsList);
    return SUCCESS;
}

void StudentsBlock_addStudent(studentsBlockPtr studentsBl, studentPtr student)
{
    List_addFirst(studentsBl->studentsList, student);
}

void StudentsBlock_deleteStudent(studentsBlockPtr studentsBl, studentPtr student)
{
    List_remove(studentsBl->studentsList, student, StudentRecord_compareID);
}

void StudentsBlock_destroy(void **studentsBl)
{
    studentsBlockPtr *studentBlock = (studentsBlockPtr *)studentsBl;
    List_destroy(&(*studentBlock)->studentsList, StudentRecord_delete);
    free(*studentBlock);
    *studentBlock = NULL;
}

int StudentsBlock_getPostcode(void *studentsBl)
{
    return ((studentsBlockPtr)studentsBl)->postcode;
}

int StudentsBlock_getCount(studentsBlockPtr studentBl)
{
    return List_returnSize(studentBl->studentsList);
}

void StudentsBlock_initIterator(studentsBlockPtr studentBl, studentIterator *it)
{
    List_initIterator(studentBl->studentsList, it);
}

void *StudentsBlock_nextValue(studentIterator *it)
{
    return List_returnValue(it);
}

int StudentsBlock_comparePostcode(void *value1, void *value2)
{
    if(((studentsBlockPtr)value1)->postcode
                        == ((studentsBlockPtr)value2)->postcode)
        return 0;
    else
        return -1;
}


/*
 * Student record
 */

int StudentRecord_create(studentPtr *record, int studid, char *firstname, char *lastname,
                        float gpa, int numofcourses, char *deprt, int postcode)
{

    /*
     * create a new student record and initialize it
     */
    if((*record = malloc(sizeof(struct studentRecord))) == NULL)
        return ALLOCATE_ERROR;

    //allocate memory for all strings
    (*record)->firstname = malloc(sizeof(char) * (strlen(firstname) + 1));
    if((*record)->firstname == NULL)
    {
        free(*record);
        return ALLOCATE_ERROR;
    }
    (*record)->lastname = malloc(sizeof(char) * (strlen(lastname) + 1));
    if((*record)->lastname == NULL)
    {
        free((*record)->firstname); free(*record);
        return ALLOCATE_ERROR;
    }
    (*record)->deprt = malloc(sizeof(char) * (strlen(deprt) + 1));
    if((*record)->deprt == NULL)
    {
        free((*record)->firstname); free((*record)->lastname); free(*record);
        return ALLOCATE_ERROR;
    }

    (*record)->studid = studid;
    (*record)->gpa = gpa;
    (*record)->numofcourses = numofcourses;
    (*record)->postcode = postcode;
    strcpy((*record)->firstname, firstname);
    strcpy((*record)->lastname, lastname);
    strcpy((*record)->deprt, deprt);
    return SUCCESS;
}

void StudentRecord_delete(void  **record)
{
    if(*record != NULL)
    {
        studentPtr *student = (studentPtr *) record;
        free((*student)->firstname);
        free((*student)->lastname);
        free((*student)->deprt);
        free(*student);
        *student = NULL;
    }
}


void StudentRecord_setGPA(studentPtr student, float gpa)
{
    student->gpa = gpa;
}

void StudentRecord_setNumCources(studentPtr student, int numcources)
{
    student->numofcourses = numcources;
}

int StudentRecord_getID(studentPtr student)
{
    return student->studid;
}

int StudentRecord_getPostcode(studentPtr student)
{
    return student->postcode;
}

float StudentRecord_getGPA(studentPtr student)
{
    return student->gpa;
}

char *StudentRecord_getDeprt(studentPtr student)
{
    return student->deprt;
}

int StudentRecord_getCourses(studentPtr student)
{
    return student->numofcourses;
}

void StudentRecord_print(studentPtr student)
{
    printf("%d %s %s %.2f %d %s %d\n", student->studid, student->lastname,
           student->firstname, student->gpa, student->numofcourses,
           student->deprt, student->postcode);
}

int StudentRecord_compareID(void *value1, void *value2)
{
    if(((studentPtr)value1)->studid == ((studentPtr)value2)->studid)
        return 0;
    else
        return -1;
}
