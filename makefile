CC = gcc
OUT = stucs
BIN_DIR = bin
SRC_DIR = src
OBJS = record.o skiplist.o main.o functions.o hashTable.o list.o
OBJS_DIR = $(addprefix $(BIN_DIR)/,$(OBJS))
SOURCE_FILES = *.c
HEADER_FILES = *.h
FLAGS = -c -Wall -o $@

all: $(OBJS_DIR)
	$(CC) -o $(BIN_DIR)/$(OUT) $(OBJS_DIR)

$(BIN_DIR)/main.o: $(SRC_DIR)/main.c
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/record.o: $(SRC_DIR)/record.c $(SRC_DIR)/record.h
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/skiplist.o: $(SRC_DIR)/skiplist.c $(SRC_DIR)/skiplist.h
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/functions.o: $(SRC_DIR)/functions.c $(SRC_DIR)/functions.h
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/hashTable.o: $(SRC_DIR)/hashTable.c $(SRC_DIR)/hashTable.h
	$(CC) $(FLAGS) $<
	
$(BIN_DIR)/list.o: $(SRC_DIR)/list.c $(SRC_DIR)/list.h
	$(CC) $(FLAGS) $<
	
$(OBJS_DIR): | $(BIN_DIR)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)
	
clean:
	rm -f $(BIN_DIR)/$(OUT) $(OBJS_DIR)
	
count:
	wc $(SOURCE_FILES) $(HEADER_FILES)
